<?php

include_once "ChiTietMay.php";
include_once "ChiTietDon.php";
include_once "ChiTietPhuc.php";
include_once "May.php";
include_once "Kho.php";

echo "*---- QUẢN LÍ CHI TIẾT MÁY ---* \n";
echo "*                             * \n";
echo "*      1. Chi tiết Đơn        * \n";
echo "*      2. Chi tiết Phức       * \n";
echo "*      3. Quản lý Máy         * \n";
echo "*      4. Quản lý Kho         * \n";
echo "*-----------------------------* \n";

$Chon = "";
echo "*-- ";
do {
    $Chon = readline("Lựa chọn của anh là (1 -> 4): ");
    echo "*------------------------------*\n";

    if ($Chon < 1 || $Chon > 4) {
        echo "Sai rồi anh, chọn từ số 1 tới 4 thôi (phải là số nguyên) -_-. \n";
    }
} while ($Chon < 1 || $Chon > 4);
switch ($Chon) {
    case 1: {
        echo "*--  CHI TIẾT ĐƠN ---*\n";
        echo "*--                --* \n";

        $ctd = new ChiTietDon();
        $ctd->nhap();
       
        $ctd->xuat();
        break;
    }
    case 2: {
        echo "*--  CHI TIẾT PHỨC  --*\n";
        echo "*--\n";
        $ctp = new ChiTietPhuc();
        $ctp->nhap();
        $ctp->xuat();
        echo "*--------Tổng tiền và khối lượng-----------*\n";
        echo "*-- Tổng tiền: " . $ctp->tinhTien() . "\n";
        echo "*-- Tổng khối lượng: " . $ctp->tinhKhoiLuong() . "\n";
        echo "*--\n\n";
        break;
    }
    case 3: {
        echo "\n\n---\n";
        echo "*----- QUẢN LÝ MÁY ----*\n";
        echo "*---\n";
        $may = new May();
        $may->Input();
        $may->Ouput();
        echo "*--Tổng tiền và khối lượng máy ---*\n";
        echo "*--   Tổng tiền: " . $may->tinhTien() . "\n";
        echo "*--   Tổng khối lượng: " . $may->tinhKhoiLuong() . "\n";
        echo "*--\n\n";
        break;
    }
    case 4: {
        echo "\n\n---\n";
        echo "*---  QUẢN LÝ KHO  ---\n";
        echo "*---\n";
        $kho = new Kho();
        $kho->Input();
        echo "*-------------------------------*\n";
        $kho->thongKeDanhSachMay();
        echo "*-------------------------------*\n";
        $kho->tongGiaMay();
        $kho->tongKhoiLuongMay();
        $kho->timMay();
        echo "*---\n\n";
        break;
    }
}

?>