<?php

abstract class ChiTietMay {
    private $maSo;

    public function getMaSo() {
        return $this->maSo;
    }

    public function nhap() {
        echo "*-- ";
        $this->maSo = readline("Nhập mã số máy: ");
    }

    public function xuat() {

        echo "\n*-- Mã số: " . $this->maSo . "\n";
    }

    abstract function tinhTien();
    abstract function tinhKhoiLuong();

}

?>

