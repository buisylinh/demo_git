<?php

class May extends ChiTietMay {
    private $MaMay;
    private $TenMay;
    private $danhSachChiTiet = array();

    public function getMaSoMay() {
        return $this->MaMay;
    }

    public function Input() {
        echo "*--";
        $this->MaMay = readline("Nhập mã số máy: ");
        echo "*--";
        $this->TenMay = readline("Nhập tên máy: ");
        echo "*--";
        do {
            $danhSachChiTiet = readline("Nhập số lượng chi tiết: ");
            if (!is_numeric($danhSachChiTiet)) {
                echo "@@ Sai. Nhập lại đi anh (bằng số) \n";
            }
        } while (!is_numeric($danhSachChiTiet));

        for ($i = 0; $i < $danhSachChiTiet; $i++) {
            $loai = null;
            echo "*-- ";
            do {
                $loai = readline("Nhập loại chi tiết (1: Chi tiết đơn | 2: Chi tiết phức): ");
                if ($loai != 1 && $loai != 2) {
                    echo " @@ Sai. Nhập lại đi anh (bằng số) \n";
                }
            } while ($loai != 1 && $loai != 2);

            $chitiet = null;
            if ($loai == 1) {
                $chitiet = new ChiTietDon();
            } else {
                $chitiet = new ChiTietPhuc();
            }
            $chitiet->nhap();
            $this->danhSachChiTiet[] = $chitiet;
        }
    }

    public function Ouput() {
        echo "*--\n*-------Thông tin máy --------------*\n";
        echo "*--    Mã số máy : ".$this->MaMay."\n";
        echo "*--    Tên máy: ".$this->TenMay."\n";
        echo "*--    Danh sách chi tiết:    ";

        for ($i = 0; $i < count($this->danhSachChiTiet); $i++) {
            echo $this->danhSachChiTiet[$i]->getMaSo() . "      ";
        }
        echo "\n";
    }

    public function tinhTien() {
        $tongTien = 0;
        for ($i = 0; $i < count($this->danhSachChiTiet); $i++) {
            $tongTien += $this->danhSachChiTiet[$i]->tinhTien();
        }
        return $tongTien;
    }

    public function tinhKhoiLuong() {
        $tongKhoiLuong = 0;
        for ($i = 0; $i < count($this->danhSachChiTiet); $i++) {
            $tongKhoiLuong += $this->danhSachChiTiet[$i]->tinhKhoiLuong();
        }
        return $tongKhoiLuong;
    }
}

?>