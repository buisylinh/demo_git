<?php

include_once "May.php";

class Kho {
    private $tenKho = "";
    private $danhSachMay = array();
    private $soLuongMay = 0;

    public function Input() {
        echo "*-- ";
        $_soLuongMayCanThem = readline("Nhập số máy cần thêm vào kho: ");
        for ($i = 0; $i < $_soLuongMayCanThem; $i++) {
            $may = new May();
            echo "*--\n";
            $may->nhap();
            $this->danhSachMay[] = $may;
            $this->soLuongMay++;
        }
    }

    public function tongGiaMay() {
        $tong = 0;
        for ($i = 0; $i < $this->soLuongMay; $i++) {
            $tong += $this->danhSachMay[$i]->tinhTien();
        }
        echo "*-- Tổng các máy có trong kho là :" . $tong . "\n";
    }

    public function tongKhoiLuongMay() {
        $tong = 0;
        for ($i = 0; $i < $this->soLuongMay; $i++) {
            $tong += $this->danhSachMay[$i]->tinhKhoiLuong();
        }
        echo "*-- Tổng khối lượng máy có trong kho là: " . $tong . "\n";
    }

    public function thongKeDanhSachMay() {
        echo "*-------------- Thống kế danh sách máy trong kho " . $this->tenKho . " -----------*\n";
        echo "*-------------------------------------------------------------*\n";
        for ($i = 0; $i < $this->soLuongMay; $i++) {
            $this->danhSachMay[$i]->xuat();
        }
    }

    public function timMay() {
        echo "*--------------------------------------------------------------------*\n";
        echo "*--";
        $res = null;
        $_msMay = readline("Nhập mã số máy cần tìm: ");
        for ($i = 0; $i < $this->soLuongMay; $i++) {
            if ($this->danhSachMay[$i]->getMaSoMay() == $_msMay) {
                $res = 1;
                break;
            } else {
                $res = 0;
            }
        }
        if ($res) {
            echo "*--Tìm thấy máy có mã " . $this->danhSachMay[$i]->getMaSoMay() . "\n";
            $this->danhSachMay[$i]->xuat();
        } else {
            echo "*--Mã số máy"  .$_msMay . " không tồn tại.\n";
        }
    }
}

?>